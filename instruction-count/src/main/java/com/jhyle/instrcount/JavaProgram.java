package com.jhyle.instrcount;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import soot.ArrayType;
import soot.RefType;
import soot.Scene;
import soot.Type;
import soot.VoidType;
import soot.options.Options;

class JavaProgram
{
	private String mainClass;
	
	public JavaProgram(String classpath, String mainClass)
	{
		Options.v().set_via_shimple(true);
		Options.v().set_whole_program(true);
		Options.v().set_output_format(Options.output_format_shimple);
		Scene.v().setSootClassPath(classpath);
		Scene.v().loadClassAndSupport(mainClass);
		Scene.v().loadNecessaryClasses();
		
		this.mainClass = mainClass;
	}
	
	public Collection<JavaClass> classes()
	{
		return JavaClass.loaded();
	}

	public Map<JavaMethod, Invocation> methods()
	{
		return Invocation.evaluatedMethods();
	}
	
	public BigInteger count(String pckg2count)
	{
		return Invocation.load(mainClass, "main", Arrays.asList(ArrayType.v(RefType.v("java.lang.String"), 1)), VoidType.v(), Arrays.asList((Set<Type>) new HashSet<Type>(Arrays.asList(ArrayType.v(RefType.v("java.lang.String"), 1))))).analyze(pckg2count).getInstructions();
	}

}
