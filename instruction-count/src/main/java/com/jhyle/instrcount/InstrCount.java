package com.jhyle.instrcount;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class InstrCount
{
	public static void main(String[] args)
	{
		if (args.length != 3) {
			System.out.println("Please provide the parameter <classPath> and <mainClass> and the <package> of the classes to count.");
			System.exit(0);
		}
		
		String classPath = args[0];
		String mainClass = args[1];
		String pckg2count = args[2];

		JavaProgram program = new JavaProgram(classPath, mainClass);
		System.out.println(String.format("%,d", program.count(pckg2count)));
		
//		Map<BigInteger, List<JavaMethod>> methods = new TreeMap<BigInteger, List<JavaMethod>>();
//		for (JavaMethod method : program.methods().keySet()) {
//			Invocation result = program.methods().get(method);
//			List<JavaMethod> list = methods.get(result.getInstructions());
//			if (list == null) {
//				list = new ArrayList<JavaMethod>();
//				methods.put(result.getInstructions(), list);
//			}
//			list.add(method);
//		}
//		
//		for (BigInteger n : methods.keySet()) if (!n.equals(BigInteger.ZERO)) {
//			for (JavaMethod method : methods.get(n)) {
//				System.out.println(String.format("%,d: %s", n, method));
//			}
//		}
	}
}
