package com.jhyle.instrcount;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import soot.G;
import soot.Scene;

public class TestExample1
{
	private String classPath = System.getProperty("java.home") + File.separator + "lib" + File.separator + "rt.jar" + File.pathSeparator
			+ System.getProperty("java.home") + File.separator + "lib" + File.separator + "jce.jar" + File.pathSeparator
			+ System.getProperty("java.class.path");
	
	@Test
	public void test()
	{
		System.out.println(new JavaProgram(classPath, "com.jhyle.instrcount.Example1").count("com.jhyle."));
	}
}
